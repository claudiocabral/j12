/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 18:06:34 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/16 12:09:05 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ft_display_file.h"

int	ft_display_file(char *path, char buffer[BUFFER_SIZE])
{
	int f_handler;
	int	size;

	f_handler = open(path, O_RDONLY);
	if (f_handler == -1)
	{
		write(2, "Failed to open file\n", 20);
		return (0);
	}
	while ((size = read(f_handler, buffer, BUFFER_SIZE)) > 0)
		write(1, buffer, size);
	if (size < 0)
	{
		write(2, "Failed to read file\n", 20);
		return (0);
	}
	if (close(f_handler) == -1)
	{
		write(2, "Failed to close file\n", 21);
		return (0);
	}
	return (1);
}

int	main(int argc, char **argv)
{
	int		status;
	char	buffer[BUFFER_SIZE];

	if (argc == 2)
		status = ft_display_file(argv[1], buffer);
	else if (argc < 2)
	{
		write(2, "File name missing.\n", 19);
		return (1);
	}
	else
	{
		write(2, "Too many arguments.\n", 20);
		return (1);
	}
	return (0);
}
