/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/09 09:25:06 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/16 11:45:53 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H

int				ft_putchar(char c);
char			**ft_split(char *str, char *charset);
char			**ft_split_whitespaces(char *str);
char			*ft_concat_params(int argc, char **argv);
char			*ft_strcapitalize(char *str);
char			*ft_strcat(char *dest, char *src);
char			*ft_strcpy(char *dest, char *src);
char			*ft_strdup(char *src);
char			*ft_strlowcase(char *str);
char			*ft_strncat(char *dest, char *src, int nb);
char			*ft_strncpy(char *dest, char *src, int len);
char			*ft_strrev(char *str);
char			*ft_strstr(char *str, char *to_find);
char			*ft_strupcase(char *str);
int				*ft_range(int min, int max);
int				ft_atoi(char *str);
int				ft_atoi_base(char *str, char *base);
int				ft_iterative_power(int nb, int power);
int				ft_sqrt(int nb);
int				ft_str_is_alpha(char *str);
int				ft_str_is_lowercase(char *str);
int				ft_str_is_numeric(char *str);
int				ft_str_is_printable(char *str);
int				ft_str_is_uppercase(char *str);
int				ft_strlen(char *str);
int				ft_strcmp(char *s1, char *s2);
int				ft_strncmp(char *s1, char *s2, unsigned int n);
int				ft_ultimate_range(int **range, int min, int max);
unsigned int	ft_strlcat(char *dest, char *src, unsigned int size);
unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);
void			ft_print_words_tables(char **tab);
void			ft_putnbr(int nb);
void			ft_putnbr_base(int nbr, char *base);
void			ft_putstr(char *str);
void			ft_putstr_non_printable(char *str);
void			ft_swap(int *a, int*b);

#endif
