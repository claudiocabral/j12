# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ccabral <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/09 19:16:08 by ccabral           #+#    #+#              #
#    Updated: 2017/08/16 11:36:13 by ccabral          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	ft
LIB_NAME	=	lib$(NAME).a
COMPILE		=	cc -Wall -Wextra -Werror
LIB_SRCS	=	srcs/ft_any.c \
				srcs/ft_atoi.c \
				srcs/ft_atoi_base.c \
				srcs/ft_count_if.c \
				srcs/ft_foreach.c \
				srcs/ft_is_sort.c \
				srcs/ft_map.c \
				srcs/ft_param_to_tab.c \
				srcs/ft_putchar.c \
				srcs/ft_putnbr.c \
				srcs/ft_putnbr_base.c \
				srcs/ft_putstr.c \
				srcs/ft_putstr_non_printable.c \
				srcs/ft_show_tab.c \
				srcs/ft_split_whitespaces.c \
				srcs/ft_str_is_alpha.c \
				srcs/ft_str_is_lowercase.c \
				srcs/ft_str_is_numeric.c \
				srcs/ft_str_is_printable.c \
				srcs/ft_str_is_uppercase.c \
				srcs/ft_strcapitalize.c \
				srcs/ft_strcat.c \
				srcs/ft_strcmp.c \
				srcs/ft_strcpy.c \
				srcs/ft_strdup.c \
				srcs/ft_strlcat.c \
				srcs/ft_strlcpy.c \
				srcs/ft_strlen.c \
				srcs/ft_strlowcase.c \
				srcs/ft_strncat.c \
				srcs/ft_strncmp.c \
				srcs/ft_strncpy.c \
				srcs/ft_strstr.c \
				srcs/ft_strupcase.c \
				srcs/ft_swap.c \
				srcs/ft_error.c

LIB_OBJ		= 	$(LIB_SRCS:srcs/%.c=obj/%.o)
INCLUDE		=	includes

all: $(NAME)

$(NAME): $(LIB_OBJ)
	ar rcs $(LIB_NAME) $(LIB_OBJ)

debug: $(LIB_SRCS)
	$(COMPILE) -g -c -I $(INCLUDE) $(LIB_SRCS)
	if [ ! -d obj ] ; then mkdir obj ; fi
	ar rcs $(LIB_NAME) $(LIB_OBJ:obj/%.o=%.o)

$(LIB_OBJ): $(LIB_SRCS)
	$(COMPILE) -c -I $(INCLUDE) $(LIB_SRCS)
	if [ ! -d obj ] ; then mkdir obj ; fi
	mv $(LIB_SRCS:srcs/%.c=%.o) obj/

error: scripts/make_error.c
	$(COMPILE) scripts/make_error.c -o make_error_header\
	    && ./make_error_header > includes/ft_error_list.h

clean:
	rm -rf obj

fclean: clean
	rm -rf $(LIB_NAME) make_error_header

re: fclean all
