/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 18:27:11 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/16 11:48:46 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strncpyj07ex04(char *dest, char *src, unsigned int size)
{
	char	*head;

	head = dest;
	while ((*dest++ = *src++) && size > 1)
		--size;
	while (size-- > 0)
		*dest++ = '\0';
	*dest = '\0';
	return (head);
}

int		ft_is_separator(char c)
{
	if (c == ' ' || c == '\t' || c == '\n')
		return (1);
	return (0);
}

int		ft_word_length(char *str)
{
	int		size;

	while (*str)
	{
		if (!ft_is_separator(*str))
			break ;
		++str;
	}
	size = 0;
	while (*str)
	{
		if (ft_is_separator(*str))
			break ;
		++size;
		++str;
	}
	return (size - 1);
}

int		ft_count_words(char *str)
{
	int	size;

	size = 0;
	while (*str)
	{
		while (!ft_is_separator(*str) && *str)
			++str;
		++size;
		while (ft_is_separator(*str) && *str)
			++str;
	}
	return (size);
}

char	**ft_split_whitespaces(char *str)
{
	char	**word;
	int		nbr_words;
	int		size;
	int		i;

	size = 0;
	i = 0;
	while (ft_is_separator(*str))
		++str;
	nbr_words = ft_count_words(str);
	word = (char **)malloc(sizeof(char *) * (nbr_words + 1));
	while (i < nbr_words)
	{
		size = ft_word_length(str) + 1;
		word[i] = (char *)malloc(sizeof(char) * size);
		ft_strncpyj07ex04(word[i], str, size);
		str += size;
		while (ft_is_separator(*str))
			++str;
		++i;
	}
	word[i] = 0;
	return (word);
}
