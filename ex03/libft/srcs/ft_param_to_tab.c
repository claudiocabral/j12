/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stock_par.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/09 10:34:52 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/10 10:13:35 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_par.h"

int					ft_strlen(char *str)
{
	int	length;

	length = 0;
	while (*str)
	{
		++length;
		++str;
	}
	return (length);
}

char				*ft_strdup(char *src)
{
	char	*head;
	char	*dup;
	int		size;

	head = src;
	size = 1;
	while (*src)
	{
		++size;
		++src;
	}
	dup = (char *)malloc(sizeof(char) * size);
	src = head;
	head = dup;
	while (*src)
		*dup++ = *src++;
	*dup = '\0';
	return (head);
}

struct s_stock_par	*ft_param_to_tab(int ac, char **av)
{
	t_stock_par	*par;
	t_stock_par	*head;
	int			i;

	par = malloc(sizeof(t_stock_par) * (ac + 1));
	head = par;
	i = 0;
	while (i < ac)
	{
		par->size_param = ft_strlen(av[i]);
		par->str = av[i];
		par->copy = ft_strdup(av[i]);
		par->tab = ft_split_whitespaces(av[i]);
		++i;
		++par;
	}
	par->str = 0;
	return (head);
}
