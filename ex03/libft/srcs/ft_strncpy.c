/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/05 17:59:53 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/08 11:49:12 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy(char *dest, char *src, unsigned int len)
{
	char			*head;

	head = dest;
	while (*src && len > 0)
	{
		*dest++ = *src++;
		--len;
	}
	while (len > 0)
	{
		*dest++ = '\0';
		--len;
	}
	return (head);
}
