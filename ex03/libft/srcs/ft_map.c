/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/09 19:26:27 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/10 11:31:04 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_map(int *tab, int length, int (*f)(int))
{
	int		*map;
	int		*head;

	map = malloc(sizeof(int) * length);
	head = map;
	while (length-- > 0)
		*map++ = (*f)(*tab++);
	return (head);
}
