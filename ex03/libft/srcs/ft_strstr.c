/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 09:20:00 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/10 13:29:50 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	char	*head;
	char	*tmp;

	tmp = to_find;
	while (*str)
	{
		head = str;
		while ((*str == *to_find) && *to_find)
		{
			++str;
			++to_find;
		}
		if (!*to_find)
			return (head);
		to_find = tmp;
		str = head + 1;
	}
	return (0);
}
