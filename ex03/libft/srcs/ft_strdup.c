/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 17:53:12 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/16 11:48:41 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	char	*head;
	char	*dup;
	int		size;

	head = src;
	size = 1;
	while (*src)
	{
		++size;
		++src;
	}
	dup = (char *)malloc(sizeof(char) * size);
	src = head;
	head = dup;
	while (*src)
		*dup++ = *src++;
	*dup = '\0';
	return (head);
}
