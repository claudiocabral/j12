/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hexdump.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/16 12:16:11 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/23 16:11:29 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include "ft.h"
#include "ft_error.h"
#include "ft_hexdump.h"
#include  <stdint.h>

void	ft_print_addr(long c)
{
	char	*hex;
	long	val;
	long	tmp;

	hex = "0123456789abcdef";
	val = 0x1;
	tmp = c / 0x10;
	while (tmp || val < 0x10000000)
	{
		tmp /= 0x10;
		val *= 0x10;
	}
	while (val)
	{
		ft_putchar(hex[c / val]);
		c %= val;
		val /= 0x10;
	}
}

int		ft_put_hexbuffer(char buffer[BUFFERSIZE], int size)
{
	static int	i = 0;
	static long	addr = 0;

	while (addr < size)
	{
		ft_print_addr(addr);
		ft_putstr("  ");
		while (i < 16 && addr + i < size)
		{
			ft_puthex(buffer[addr + i]);
			ft_putchar(' ');
			++i;
			if (i == 8)
				ft_putchar(' ');
		}
		ft_putchar(' ');
		while (i++ < 16)
		{
			if (i == 8)
				ft_putchar(' ');
			ft_putstr("   ");
		}
		i = 0;
		ft_putchar('|');
		while (i < 16 && addr + i < size)
			ft_putstr_or_dot(buffer[addr + i++]);
		ft_putchar('|');
		ft_putchar('\n');
		while (addr < size
				&& *(long *)(buffer + addr) == *(long *)(buffer + addr + 0x10))
		{
			addr += 0x10;
			if ((long)*(buffer + addr) != (long)*(buffer + addr + 0x10))
				ft_putstr("*\n");
		}
		addr += i;
		i = 0;
	}
	return (1);
}

int		ft_hexdump(char *path, char buffer[BUFFERSIZE])
{
	int		f_handler;
	int		size;
	if((f_handler = open(path, O_RDONLY)) == -1 )
		return (ft_error_msg(path, ": "));
	while ((size = read(f_handler, buffer, BUFFERSIZE)) > 0)
		ft_put_hexbuffer(buffer, size);
	ft_put_hex
	close(f_handler);
	return (1);
}

int		main(int argc, char **argv)
{
	int		i;
	char	buffer[BUFFERSIZE];

	i = 1;
	if (argc > 2)
	{
		if (ft_strcmp(argv[1], "-C") == 0)
			while (++i < argc)
				ft_hexdump(argv[i], buffer);
	}
	return (0);
}
