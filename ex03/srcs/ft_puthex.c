/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puthex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/16 12:17:50 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/23 16:08:32 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>
#include <ft_hexdump.h>

void	ft_puthex(char c)
{
	static char hex_table[16] = "0123456789abcdef";

	{
		ft_putchar(hex_table[c / 16]);
		ft_putchar(hex_table[c % 16]);
	}
}

void	ft_putstr_or_dot(char c)
{

	if (c >= ' ' && c < 127)
	{
		ft_putchar(c);
	}
	else
		ft_putchar('.');
}
