/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tail.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 18:06:41 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/21 11:23:39 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include "ft.h"
#include "ft_error.h"
#include "ft_tail.h"

int		ft_lseek(int f_handler, int offset)
{
	char	buffer[256];

	while (offset--)
		read(f_handler, buffer, 1);
	return (1);
}

int		ft_tail_minus(char *path, int offset, char buffer[BUFFER_SIZE])
{
	int	size;
	int	file_size;
	int f_handler;

	f_handler = open(path, O_RDONLY);
	size = 0;
	file_size = 0;
	while ((size = read(f_handler, buffer, BUFFER_SIZE)))
		file_size += size;
	if (offset < 0)
		offset = -offset;
	offset = file_size - offset;
	close(f_handler);
	f_handler = open(path, O_RDONLY);
	if (offset < file_size && offset > 0)
		ft_lseek(f_handler, offset);
	while ((size = read(f_handler, buffer, BUFFER_SIZE)) > 0)
		write(1, buffer, size);
	if (size < 0)
		return (ft_error_msg("read: ", ""));
	if (close(f_handler) == -1)
		return (ft_error_msg("close: ", ""));
	return (1);
}

int		ft_tail_plus(char *path, int offset, char buffer[BUFFER_SIZE])
{
	int	size;
	int	f_handler;

	f_handler = open(path, O_RDONLY);
	if (ft_lseek(f_handler, offset - 1) == -1)
		return (ft_error_msg("ft_lseek: ", "has problems"));
	while ((size = read(f_handler, buffer, BUFFER_SIZE)) > 0)
		write(1, buffer, size);
	if (size < 0)
		return (ft_error_msg("read: ", ""));
	if (close(f_handler) == -1)
		return (ft_error_msg("close: ", ""));
	return (1);
}

t_tail	ft_argc_switch(int argc, char **argv)
{
	t_tail	tail;

	tail.f = &ft_tail_minus;
	if (argc == 1)
		return (tail);
	else if (ft_strcmp(argv[1], "-c") == 0)
	{
		if (argv[2])
		{
			if (argv[2][0] == '+')
				tail.f = &ft_tail_plus;
			tail.offset = ft_atoi(argv[2]);
		}
	}
	else
	{
		if (argv[1][2] == '+')
			tail.f = &ft_tail_plus;
		tail.offset = ft_atoi(argv[1] + 2);
	}
	return (tail);
}
