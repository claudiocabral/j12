/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/16 17:35:42 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/23 14:11:23 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include "ft.h"
#include "ft_error.h"
#include "ft_tail.h"

void	ft_print_name(char *path)
{
	ft_putstr("==> ");
	ft_putstr(path);
	ft_putstr(" <==\n");
}

int		ft_open(char *path)
{
	int	f_handler;

	f_handler = open(path, O_RDONLY);
	if (f_handler == -1)
		ft_error_msg(path, ": ");
	return (f_handler);
}

int		ft_run_tail(t_tail tail, int argc, char **argv)
{
	int		i;
	int		f_handler;
	char	buffer[BUFFER_SIZE];

	i = 2;
	if (argc > 3)
		ft_print_name(argv[i]);
	if ((f_handler = ft_open(argv[i])) != -1)
		 tail.f(argv[i], tail.offset, buffer);
	++i;
	while (i < argc)
	{
		if ((f_handler = ft_open(argv[i])) != -1)
		{
			ft_putchar('\n');
			ft_print_name(argv[i]);
			tail.f(argv[i], tail.offset, buffer);
		}
		++i;
	}
	return (0);
}

int		ft_put_tail_error(char *path)
{
	ft_puterror("tail: illegal offset -- ");
	ft_puterror(path);
	return (ft_puterror("\n"));
}

int		main(int argc, char **argv)
{
	t_tail	tail;

	tail.offset = 0;
	if (argv[1][0] == '-' && argv[1][1] == 'c')
	{
		tail = ft_argc_switch(argc, argv);
		if (argc > 2 && !argv[1][2])
		{
			if (!ft_str_is_numeric_or_sign(argv[2]))
				return (ft_put_tail_error(argv[2]));
			++argv;
			--argc;
		}
		else if ((argv[1][1] == 'c') && !ft_str_is_numeric_or_sign(argv[1] + 2))
			return (ft_put_tail_error(argv[1] + 2));
		else if (argv[1][2] == 0)
			return (ft_puterror("tail: option requires an argument -- c\n"));
	}
	return (ft_run_tail(tail, argc, argv));
}
