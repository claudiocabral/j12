/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tail.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 18:06:24 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/17 12:44:58 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TAIL_H
# define FT_TAIL_H

# define BUFFER_SIZE 2048

typedef	struct	s_tail
{
	int	(*f)(char *path, int offset, char buffer[BUFFER_SIZE]);
	int	offset;
}				t_tail;

t_tail			ft_argc_switch(int argc, char **argv);

#endif
