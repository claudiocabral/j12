#!/bin/zsh

diff <( ./ft_tail -c asd 2>&1 ) <( tail -c asd 2>&1 )
diff <( ./ft_tail -casd 2>&1 ) <( tail -casd 2>&1 )
diff <( ./ft_tail -c 10 asd 2>&1 ) <( tail -c 10 asd 2>&1 )
diff <( ./ft_tail -c10 asd 2>&1 ) <( tail -c10 asd 2>&1 )

for i in {1..1000} ; do
diff <( ./ft_tail -c$i Makefile ) <( tail -c$i Makefile )
done
for i in {1..1000} ; do
diff <( ./ft_tail -c $i Makefile ) <( tail -c $i Makefile )
done
for i in {1..1000} ; do
diff <( ./ft_tail -c $i Makefile Makefile askhdbask Makefile 2>&1 ) <( tail -c $i Makefile Makefile askhdbask Makefile 2>&1 )
done
for i in {1..1000} ; do
diff <( ./ft_tail -c +$i Makefile ) <( tail -c +$i Makefile )
done
for i in {1..1000} ; do
diff <( ./ft_tail -c+$i Makefile ) <( tail -c+$i Makefile )
done
