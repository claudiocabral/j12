/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 12:31:27 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/09 18:08:58 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void			ft_cpynstr(char **dest, char **src, unsigned int size)
{
	while (**src && size > 0)
	{
		**dest = **src;
		++(*dest);
		++(*src);
		--size;
	}
	while (size-- > 0)
	{
		**dest = '\0';
		++(*dest);
	}
}

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int	count;
	unsigned int	src_size;
	unsigned int	temp_size;
	char			*temp;

	count = 0;
	src_size = 0;
	temp_size = size;
	temp = src;
	while (*src++)
		src_size++;
	while (*dest && size)
	{
		++dest;
		--size;
		++count;
	}
	if (src_size + count > temp_size + src_size)
		return (temp_size + src_size);
	if (size)
		--size;
	ft_cpynstr(&dest, &temp, size);
	return (src_size + count);
}
