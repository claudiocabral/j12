/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 09:57:25 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/06 10:17:12 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned char	a;
	unsigned char	b;

	while (*s1 == *s2 && --n)
	{
		if (*s1 == '\0')
			break ;
		++s1;
		++s2;
	}
	a = *s1;
	b = *s2;
	if (a > b)
		return (1);
	else if (a == b)
		return (0);
	else
		return (-1);
}
