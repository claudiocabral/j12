/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/09 10:57:36 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/10 10:18:51 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"

void	ft_putstr(char *c)
{
	while (*c)
	{
		ft_putchar(*c);
		++c;
	}
}

void	ft_putnbr(int nb)
{
	long	last_digit;
	long	nmb_digits;
	long	zeros;

	zeros = 1;
	nmb_digits = 1;
	last_digit = nb;
	while (last_digit > 9 || last_digit < -9)
	{
		++nmb_digits;
		zeros *= 10;
		last_digit /= 10;
	}
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	while (nmb_digits--)
	{
		ft_putchar(nb / zeros + '0');
		nb %= zeros;
		zeros /= 10;
	}
}

void	ft_print_words_tables(char **tab)
{
	while (*tab)
	{
		ft_putstr(*tab);
		++tab;
		ft_putchar('\n');
	}
}

void	ft_show_tab(struct s_stock_par *par)
{
	while (par->str)
	{
		ft_putstr(par->copy);
		ft_putchar('\n');
		ft_putnbr(par->size_param);
		ft_putchar('\n');
		ft_print_words_tables(par->tab);
		++par;
	}
}
