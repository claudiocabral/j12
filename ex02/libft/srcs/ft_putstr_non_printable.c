/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_str_non_printable.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 16:06:33 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/16 11:48:22 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	ft_print_hex(char c)
{
	char	hex[16];
	int		i;

	i = -1;
	while (++i < 10)
		hex[i] = i + '0';
	hex[10] = 'a';
	hex[11] = 'b';
	hex[12] = 'c';
	hex[13] = 'd';
	hex[14] = 'e';
	hex[15] = 'f';
	if (c >= 16)
		ft_putchar(hex[c / 16]);
	else
		ft_putchar('0');
	ft_putchar(hex[c % 16]);
}

void	ft_putstr_non_printable(char *str)
{
	while (*str)
	{
		if (*str >= ' ' && *str != 127)
			ft_putchar(*str);
		else if (*str < 0)
		{
			ft_putchar(0);
			ft_putchar(0);
		}
		else
		{
			ft_putchar('\\');
			ft_print_hex(*str);
		}
		++str;
	}
}
