/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 18:06:28 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/21 11:02:16 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <errno.h>
#include "ft_error_list.h"
#include "ft_error.h"

int		ft_puterror(char *err)
{
	while (*err)
		write(2, err++, 1);
	return (0);
}

int		ft_error_msg(char *msg1, char *msg2)
{
	char	*err;

	err = g_error_table[errno - 1];
	ft_puterror("tail: ");
	ft_puterror(msg1);
	ft_puterror(msg2);
	ft_puterror(err);
	ft_puterror("\n");
	return (0);
}
