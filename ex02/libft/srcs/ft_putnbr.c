/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/02 14:32:38 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/16 11:48:31 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	ft_putnbr(int nb)
{
	int digit;
	int	last_digit;
	int	nmb_digits;
	int	power_10[21];

	power_10[0] = 1;
	nmb_digits = 0;
	last_digit = nb;
	while (last_digit > 9 || last_digit < -9)
	{
		nmb_digits++;
		power_10[nmb_digits] = power_10[nmb_digits - 1] * 10;
		last_digit /= 10;
	}
	power_10[nmb_digits + 1] = power_10[nmb_digits] * 10;
	nb < 0 ? ft_putchar('-') : (last_digit *= 1);
	nb < 0 ? last_digit *= -1 : (last_digit *= 1);
	ft_putchar(last_digit + 48);
	while (nmb_digits--)
	{
		digit = (nb % power_10[nmb_digits + 1]) / power_10[nmb_digits];
		digit > 0 ? ft_putchar(digit + 48) : ft_putchar(digit * -1 + 48);
	}
}
