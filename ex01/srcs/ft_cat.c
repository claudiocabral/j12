/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 18:06:28 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/18 13:46:25 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ft_error.h"
#include "ft_cat.h"

void	ft_putstr(char *str)
{
	while (*str)
		write(1, str++, 1);
}

void	ft_puterror(char *err)
{
	while (*err)
		write(2, err++, 1);
}

int		ft_error_msg(char *msg1, char *msg2)
{
	char	*err;

	err = g_error_table[errno - 1];
	ft_puterror("cat: ");
	ft_puterror(msg1);
	ft_puterror(msg2);
	ft_puterror(err);
	ft_puterror("\n");
	return (0);
}

int		ft_cat(char *path, char buffer[BUFFER_SIZE])
{
	int	f_handler;
	int	size;

	f_handler = open(path, O_RDONLY);
	if (f_handler == -1)
		return (ft_error_msg(path, ": "));
	while ((size = read(f_handler, buffer, BUFFER_SIZE)) > 0)
		write(1, buffer, size);
	if (size < 0)
		return (ft_error_msg("", ""));
	if (close(f_handler) == -1)
		return (ft_error_msg("", ""));
	return (1);
}

int		main(int argc, char **argv)
{
	int		i;
	int		has_failed;
	char	buffer[BUFFER_SIZE];

	has_failed = 0;
	if (argc == 1)
		return (1);
	i = 0;
	while (++i < argc)
		has_failed += ft_cat(argv[i], buffer);
	return (has_failed != 0);
}
